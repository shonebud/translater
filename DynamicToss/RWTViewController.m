//
//  RWTViewController.m
//  DynamicToss
//
//  Created by Main Account on 7/13/14.
//  Copyright (c) 2014 Razeware LLC. All rights reserved.
//

#import "RWTViewController.h"
#import "BHCollectionViewController.h"

static const CGFloat ThrowingThreshold = 1000;
static const CGFloat ThrowingvelocityPadding = 35;
static const CGFloat vThrowingAccuracy = 150;


@interface RWTViewController (){
    UIView *curView;
}

@property (nonatomic, assign) CGRect originalBounds;
@property (nonatomic, assign) CGPoint originalCenter;

@property (nonatomic) UIDynamicAnimator *animator;
@property (nonatomic) UIAttachmentBehavior *attachmentBehavior;
@property (nonatomic) UIPushBehavior *pushBehavior;
@property (nonatomic) UIDynamicItemBehavior *itemBehavior;
@property (nonatomic) UIGravityBehavior* gravityBehavior;

@end

@implementation RWTViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configureGestures];
    
    self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    
    //set targetPoint for each item in magnitude effect
    self.targetPoint = CGPointMake(self.view.center.x, ITEM_SIZE.height / 2.0 + 20);
    self.originalBounds = CGRectMake(0, 0, ITEM_SIZE.width, ITEM_SIZE.height);
    self.originalCenter = self.view.center;
    [self createItems];
}

- (void)createItems{
    NSArray *colors = @[[UIColor redColor],[UIColor greenColor],[UIColor blueColor]];
    self.items = [NSMutableArray new];
    for (int i = 0; i < [colors count]; i++) {
        UIView *view = [UIView new];
        
        CGRect tFrame = view.frame;
        tFrame.origin.x = arc4random_uniform(self.view.frame.size.width) - ITEM_SIZE.width / 2.0;
        tFrame.size = ITEM_SIZE;
        view.frame = tFrame;
        
        [view setBackgroundColor:colors[i]];
        [self.view addSubview:view];
        
        [_items addObject:view];
    }
}

- (void)doGravityBehaviorAnimation{
    
    for (UIView *item in self.items) {
        [self addGravityBehaviorToItem:item];
    }
}

- (void)configureGestures{
    UIPanGestureRecognizer *gesture = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(handleAttachmentGesture:)];
    gesture.minimumNumberOfTouches = 1;
    [self.view addGestureRecognizer:gesture];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction) handleAttachmentGesture:(UIPanGestureRecognizer*)gesture
{
    CGPoint boxLocation;
    CGPoint location = [gesture locationInView:self.view];
    
    if (gesture.state == UIGestureRecognizerStateBegan) {
        for (NSInteger i = _items.count - 1; i >= 0; i--) {
            UIView *item = _items[i];
            boxLocation = [gesture locationInView:item];
            if (boxLocation.x > 0 && boxLocation.x < ITEM_SIZE.width && boxLocation.y > 0 && boxLocation.y < ITEM_SIZE.height) {
                curView = item;
                break;
            }
        }
    }
    if (!curView) {
        return;
    }
    
    switch (gesture.state) {
    case UIGestureRecognizerStateBegan:{
        [self.animator removeAllBehaviors];
        
            // Create an attachment binding the anchor point (the finger's current location)
            // to a certain position on the view (the offset)
            UIOffset centerOffset = UIOffsetMake(boxLocation.x - CGRectGetMidX(curView.bounds),
                                                 boxLocation.y - CGRectGetMidY(curView.bounds));
            self.attachmentBehavior = [[UIAttachmentBehavior alloc] initWithItem:curView
                                                                offsetFromCenter:centerOffset
                                                                attachedToAnchor:location];
        
            // Tell the animator to use this attachment behavior
            [self.animator addBehavior:self.attachmentBehavior];

        break;
    }
    case UIGestureRecognizerStateEnded: {
        [self.animator removeBehavior:self.attachmentBehavior];

        //1
        CGPoint velocity = [gesture velocityInView:self.view];
        CGFloat magnitude = sqrtf((velocity.x * velocity.x) + (velocity.y * velocity.y));

        if (magnitude > ThrowingThreshold) {
            //2
            UIPushBehavior *pushBehavior = [[UIPushBehavior alloc] initWithItems:@[curView]
                                                                            mode:UIPushBehaviorModeInstantaneous];
            pushBehavior.pushDirection = CGVectorMake((velocity.x / 10), (velocity.y / 10));
            pushBehavior.magnitude = magnitude / ThrowingvelocityPadding;

            self.pushBehavior = pushBehavior;
            [self.animator addBehavior:self.pushBehavior];
            
            //4
            if (velocity.y < -100) {
                [self performSelector:@selector(moveToPointItem:) withObject:curView afterDelay:.3];
            }else{
                [self addGravityBehaviorToItem:curView];
            }
        }
        else {
            if (fabs(curView.center.x - _targetPoint.x) < vThrowingAccuracy && fabs(curView.center.y - _targetPoint.y) < vThrowingAccuracy) {
                [self moveToPointItem:curView];
            }
            curView = nil;
        }

        break;
    }
        default:{
            CGPoint locationInView = [gesture locationInView:self.view];
            [self.attachmentBehavior setAnchorPoint:locationInView];
            break;
        }
    }
}

- (void)moveToPointItem:(UIView*)item{
    UISnapBehavior *snapBehaviour = [[UISnapBehavior alloc] initWithItem:item snapToPoint:_targetPoint];
    snapBehaviour.damping = 0.65f;
    [self.animator addBehavior:snapBehaviour];
}


- (void)addGravityBehaviorToItem:(UIView*)item{
    
    UIGravityBehavior *gravityBehavior = [[UIGravityBehavior alloc] initWithItems:@[item]];
    gravityBehavior.magnitude = 1.5;
    UICollisionBehavior *collisionBehavior = [[UICollisionBehavior alloc] initWithItems:@[item]];
    
    CGFloat offset = (ITEM_SIZE.width / 2.0);
    [collisionBehavior addBoundaryWithIdentifier:@"boundFrame" forPath:[UIBezierPath bezierPathWithRect:CGRectMake(-offset, -ITEM_SIZE.height, self.view.bounds.size.width + offset * 2, self.view.bounds.size.height + ITEM_SIZE.height)]];
    
    [self.animator addBehavior:collisionBehavior];
    [self.animator addBehavior:gravityBehavior];
}


@end
