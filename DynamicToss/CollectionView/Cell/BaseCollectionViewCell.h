//
//  BaseCollectionViewCell.h
//  CollectionViewTutorial
//
//  Created by Vitalii Todorovych on 10.04.15.
//  Copyright (c) 2015 Bryan Hansen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseCollectionViewCell : UICollectionViewCell

@end
