//
//  BaseCollectionViewCell.m
//  CollectionViewTutorial
//
//  Created by Vitalii Todorovych on 10.04.15.
//  Copyright (c) 2015 Bryan Hansen. All rights reserved.
//

#import "BaseCollectionViewCell.h"

@implementation BaseCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (!self) return nil;
    
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self.class) owner:self options:nil];
    self = nib[0];
    
    return self;
}

- (void)awakeFromNib {
    // Initialization code
}

- (NSString*)reuseIdentifier{
    return NSStringFromClass([self class]);
}

@end
