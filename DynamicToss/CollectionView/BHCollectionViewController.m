//
//  BHCollectionViewController.m
//  CollectionViewTutorial
//
//  Created by Bryan Hansen on 11/3/12.
//  Copyright (c) 2012 Bryan Hansen. All rights reserved.
//

#import "BHCollectionViewController.h"
#import "BHPhotoAlbumLayout.h"
#import "BHAlbum.h"
#import "BHAlbumTitleReusableView.h"
#import "BaseCollectionViewCell.h"

static NSString * const CellIdentifier = @"BaseCollectionViewCell";
static NSString * const AlbumTitleIdentifier = @"AlbumTitle";

static NSInteger const kCellSpacing = 10;

@interface BHCollectionViewController (){
    UIScrollView *collectionScrollView;
}

@property (nonatomic, weak) IBOutlet BHPhotoAlbumLayout *photoAlbumLayout;
@property (nonatomic, weak) IBOutlet UICollectionView   *collectionView;
@property (weak, nonatomic) IBOutlet UITextField *textFld;

@property (nonatomic, strong) NSMutableArray    *albums;
@property (nonatomic, strong) NSOperationQueue  *thumbnailQueue;

@end

@implementation BHCollectionViewController

#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self loadData];
    [self configureUI];
    collectionScrollView = self.collectionView;
    collectionScrollView.delegate = self;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self runStartAnimation];
    
    
    
    
    
    NSString *url = [NSString stringWithFormat:@"https://translate.yandex.net/api/v1.5/tr.json/translate?key=trnsl.1.1.20150605T142802Z.2d6bc66aac1c33a1.1d57ca2ac96bb61359328eb94ef7f3d55af91102&lang=en-ru&text=gool"
                     //                     [fromLanguageCode lowercaseString],
                     //                     [toLanguageCode lowercaseString],
                     //                     text
                     ];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                       timeoutInterval:10];
    [request setHTTPMethod: @"GET"];
    NSError *requestError;
    NSURLResponse *urlResponse = nil;
    
    
    NSData *response1 = [NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&requestError];
    
//    NSDictionary *response = (NSDictionary*)[[JSONDecoder decoder] objectWithData:response1];
    //    [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]
    //    NSString *translate = [response[@"text"]]
//    [self.textLbl setText:[NSString stringWithFormat:@"%@",[response[@"text"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
    [self.textFld setText:[NSString stringWithUTF8String:[response1 bytes]]];
    NSLog(@"%@",[NSString stringWithUTF8String:[response1 bytes]]);
    
}

- (void)runStartAnimation{
    //run start animation
    self.collectionView.alpha = 0;
    [self doGravityBehaviorAnimation];
    [UIView animateWithDuration:1.5 animations:^{
        self.collectionView.alpha = 1;
    }];
}

- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    [self updateUI];
}

- (void)updateUI{
    [self configureLayoutOffset];
    [self configureMagnitudePoint];
}

- (IBAction)saveAction:(id)sender{
    BHAlbum *curAlbum = self.albums[[self currentCellIndex].row];
    if (curAlbum) {
        for (UIView *item in self.items) {
            [curAlbum addPhoto:item];
            [item removeFromSuperview];
        }
        [self.collectionView reloadData];
    }
}

#pragma mark Magnitude
- (void)configureMagnitudePoint{
    self.targetPoint = [self centralCellPositionInCollectionView];
}

- (CGPoint)centralCellPositionInCollectionView{
    UICollectionViewCell *centralCell = [self.collectionView cellForItemAtIndexPath:[self currentCellIndex]];
   
    CGRect centralCellRect =  [self.collectionView convertRect:centralCell.frame toView:self.view];
    CGPoint centralCellPosition = CGPointMake(centralCellRect.origin.x + centralCellRect.size.width / 2.0, centralCellRect.origin.y + centralCellRect.size.height / 2.0);
    return centralCellPosition;
}

- (NSIndexPath*)currentCellIndex{
    NSIndexPath *centralCellIndexPath = [self.collectionView indexPathForItemAtPoint:
                                         CGPointMake(self.collectionView.center.x + self.collectionView.contentOffset.x,
                                                     self.collectionView.center.y + self.collectionView.contentOffset.y)];
    return centralCellIndexPath;
}

#pragma mark
#pragma mark load data

- (void)loadData{
    self.albums = [NSMutableArray array];
    
    NSInteger photoIndex = 0;
    
    for (NSInteger a = 0; a < 10; a++) {
        BHAlbum *album = [[BHAlbum alloc] init];
        album.name = [NSString stringWithFormat:@"Photo Album %ld",a + 1];
        
        NSUInteger photoCount = arc4random()%4 + 2;
        for (NSInteger p = 0; p < photoCount; p++) {
            
            [album addPhoto:[NSObject new]];
            
            photoIndex++;
        }
        
        [self.albums addObject:album];
    }
}

#pragma mark
#pragma mark configure ui

- (void)configureUI{
    [self configureCollectionLayout];
    [self configureCollectionView];
}

- (void)configureCollectionView{
    [self.collectionView registerClass:[BaseCollectionViewCell class]
            forCellWithReuseIdentifier:CellIdentifier];
    [self.collectionView registerClass:[BHAlbumTitleReusableView class]
            forSupplementaryViewOfKind:BHPhotoAlbumLayoutAlbumTitleKind
                   withReuseIdentifier:AlbumTitleIdentifier];
}

- (void)configureCollectionLayout{
    self.photoAlbumLayout.isHorizontalDirection = YES;
    self.photoAlbumLayout.interItemSpacingX = kCellSpacing * 2;
    self.collectionView.collectionViewLayout = self.photoAlbumLayout;
}

- (void)configureLayoutOffset{
    CGFloat edgeOffsets = (self.collectionView.frame.size.width - self.photoAlbumLayout.itemSize.width) / 2.0;
    UIEdgeInsets layoutOffset = self.photoAlbumLayout.itemInsets;
    layoutOffset.left = edgeOffsets;
    layoutOffset.right = edgeOffsets;
    self.photoAlbumLayout.itemInsets = layoutOffset;
}

#pragma mark - View Rotation

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    [self configureCollectionLayout];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return self.albums.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    BHAlbum *album = self.albums[section];
    
    return album.photos.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    BaseCollectionViewCell *photoCell =
        [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier
                                                  forIndexPath:indexPath];
    return photoCell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
           viewForSupplementaryElementOfKind:(NSString *)kind
                                 atIndexPath:(NSIndexPath *)indexPath;
{
    BHAlbumTitleReusableView *titleView =
        [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                           withReuseIdentifier:AlbumTitleIdentifier
                                                  forIndexPath:indexPath];
    
    BHAlbum *album = self.albums[indexPath.section];
    
    titleView.titleLabel.text = album.name;
    
    return titleView;
}


#pragma mark - scrolling delegating

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
{
    CGFloat targetX = scrollView.contentOffset.x + velocity.x * 200.0;
    CGFloat targetIndex = round(targetX / (ITEM_SIZE.width + kCellSpacing * 2));
    if (targetIndex < 0)
        targetIndex = 0;
    targetContentOffset->x = targetIndex * (ITEM_SIZE.width + kCellSpacing * 2);
}

@end
