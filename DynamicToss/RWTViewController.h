//
//  RWTViewController.h
//  DynamicToss
//
//  Created by Main Account on 7/13/14.
//  Copyright (c) 2014 Razeware LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#define ITEM_SIZE CGSizeMake(150, 150)

@interface RWTViewController : UIViewController

@property (nonatomic, strong) NSMutableArray  *items;
@property (nonatomic, assign) CGPoint targetPoint;

- (void)doGravityBehaviorAnimation;

@end
