//
//  TodayViewController.m
//  AddWord
//
//  Created by Vitalii Todorovych on 05.06.15.
//  Copyright (c) 2015 Razeware LLC. All rights reserved.
//

#import "TodayViewController.h"
#import <NotificationCenter/NotificationCenter.h>
#import "JSONKit.h"

static NSString *fileStoreName = @"WordsList.txt";

@interface TodayViewController () <NCWidgetProviding>

@property (weak, nonatomic) IBOutlet UILabel *textLbl;
@property (weak, nonatomic) IBOutlet UIButton *rememberBtn;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end

@implementation TodayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.preferredContentSize = CGSizeMake(320, 50);
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if ([UIPasteboard generalPasteboard].string) {
        [self.textLbl setText:[UIPasteboard generalPasteboard].string];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, (unsigned long)NULL), ^(void) {
            [self loadTranslateText:self.textLbl.text fromLanguageCode:@"en" toLanguageCode:@"ru" withDelegate:self];
        });
    }else{
        [self.textLbl setText:@"No text for translate"];
    }
}

- (void)widgetPerformUpdateWithCompletionHandler:(void (^)(NCUpdateResult))completionHandler {
    // Perform any setup necessary in order to update the view.
    
    // If an error is encountered, use NCUpdateResultFailed
    // If there's no update required, use NCUpdateResultNoData
    // If there's an update, use NCUpdateResultNewData

    completionHandler(NCUpdateResultNewData);
}

#pragma mark loading data functions
- (void)loadTranslateText:(NSString*)text fromLanguageCode:(NSString*)fromLanguageCode toLanguageCode:(NSString*)toLanguageCode withDelegate:(id)_delegate{
    
    if (!text || !text.length) {
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            [self.textLbl setText:@"No text for translate"];
            [self.rememberBtn setHidden:YES];
            [self stopLooadingAnimation];
        });
        return;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        [self startLooadingAnimation];
    });
    NSString *url = [NSString stringWithFormat:@"https://translate.yandex.net/api/v1.5/tr.json/translate?key=trnsl.1.1.20150605T142802Z.2d6bc66aac1c33a1.1d57ca2ac96bb61359328eb94ef7f3d55af91102&lang=%@-%@&text=%@",
                     fromLanguageCode,
                     toLanguageCode,
                     text
                     ];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                       timeoutInterval:10];
    [request setHTTPMethod: @"GET"];
    NSError *requestError;
    NSURLResponse *urlResponse = nil;
    
    
    NSData *response1 = [NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&requestError];
    if (response1) {
        NSDictionary *response = (NSDictionary*)[[JSONDecoder decoder] objectWithData:response1];
        if (response && [response objectForKey:@"text"]) {
            NSArray *translatesArr = [response objectForKey:@"text"];
            
            if (translatesArr && translatesArr.count) {
                NSMutableString *translateStr = [NSMutableString new];
                for (NSString *translateVariable in translatesArr) {
                    [translateStr appendString:[NSString stringWithFormat:@"%@\n",translateVariable]];
                }

                dispatch_async(dispatch_get_main_queue(), ^(void) {
                    [self.textLbl setText:translateStr];
                    [UIView animateWithDuration:.3 animations:^{
                        [self.rememberBtn setEnabled:YES];
                        [self.rememberBtn setTitle:@"Save" forState:UIControlStateNormal];
                    }];
                });
            }
            
        }
    }
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        [self stopLooadingAnimation];
    });
}

- (void)startLooadingAnimation{
    [self.activityIndicator startAnimating];
    [self.rememberBtn setEnabled:NO];
    [self.rememberBtn setHidden:NO];
    [self.rememberBtn setTitle:@"Translating..." forState:UIControlStateNormal];
}


- (void)stopLooadingAnimation{
    [self.activityIndicator stopAnimating];
}

- (IBAction)onRememberAction:(id)sender {
    [self addWord:self.textLbl.text];
    
    [UIView animateWithDuration:.5 animations:^{
        self.textLbl.alpha = 0;
        self.rememberBtn.hidden = YES;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:.3 animations:^{
            [self.textLbl setText:@"Saved"];
            self.textLbl.alpha = 1;
        }];
    }];
}

- (void)addWord:(NSString*)str{
    
    NSString * path = [self pathStr];
    NSMutableArray *list = [[NSMutableArray alloc] initWithContentsOfFile:path];
    if (!list) {
        list = [NSMutableArray new];
    }
    [list addObject:str];
    [list writeToFile:path atomically:YES];
    [[NSUserDefaults standardUserDefaults] setObject:list forKey:fileStoreName];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (IBAction)fatch:(id)sender {
    
    NSMutableArray *list = [[NSMutableArray alloc] initWithContentsOfFile:[self pathStr]];
    if (!list) {
        list = [[NSUserDefaults standardUserDefaults] objectForKey:fileStoreName];
    }
    if (list) {
        NSString *listStr = [list componentsJoinedByString:@""];
        [[UIPasteboard generalPasteboard] setString:listStr];
    }
}

- (NSString*)pathStr{
    
    NSString * path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    path = [path stringByAppendingString:fileStoreName];
    return path;
}

@end
